<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\Calculator;

use Api\ReadModel\CurrencyInterface;
use Api\ReadModel\MoneyInterface;

/**
 * CalculatorInterface defines public api for currency rate calculators.
 */
interface CalculatorInterface
{
    /**
     * @param CurrencyInterface $currencyFrom
     * @param CurrencyInterface $currencyTo
     * @return string
     */
    public function calculateExchangeRate(CurrencyInterface $currencyFrom, CurrencyInterface $currencyTo): string;

    /**
     * @param MoneyInterface $moneyFrom
     * @param CurrencyInterface $currencyTo
     * @return string
     */
    public function calculateExchangeCurrencyAmount(MoneyInterface $moneyFrom, CurrencyInterface $currencyTo): string;
}