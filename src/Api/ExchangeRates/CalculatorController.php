<?php

declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\Dto\CalculatorExchangeDtoFactory;
use Api\ReadModel\Money;
use Api\Response;
use Api\ResponseInterface;
use Api\Service\ExchangeRates\Calculator\CalculatorInterface;
use Api\Service\ExchangeRates\DataProvider\DataProviderInterface;
use Api\Validator\Constraint\ConstraintException;
use Api\Validator\ValidatorInterface;
use RuntimeException;
use Throwable;

/**
 * Controller for currency calculator.
 */
class CalculatorController
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var DataProviderInterface
     */
    private $dataProvider;

    /**
     * @var CalculatorInterface
     */
    private $calculator;

    /**
     * @param ValidatorInterface $validator
     * @param DataProviderInterface $dataProvider
     * @param CalculatorInterface $calculator
     */
    public function __construct(
        ValidatorInterface $validator,
        DataProviderInterface $dataProvider,
        CalculatorInterface $calculator
    )
    {
        $this->validator = $validator;
        $this->dataProvider = $dataProvider;
        $this->calculator = $calculator;
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
        try {
            $payload = CalculatorExchangeDtoFactory::create($inputArray);

            $this->validator->validate($payload);

            $currencyFrom = $this->dataProvider->provideCurrencyRateFor($payload->getFromCurrency());
            $currencyTo = $this->dataProvider->provideCurrencyRateFor($payload->getToCurrency());

            $currencyExchangeRate = $this->calculator->calculateExchangeRate($currencyFrom, $currencyTo);
            $currencyExchangeAmount = $payload->getAmount() !== null
                ? $this->calculator->calculateExchangeCurrencyAmount(new Money($payload->getAmount(), $currencyFrom), $currencyTo)
                : null;

            return new Response([
                'resulting_rate' => $currencyExchangeRate,
                'resulting_amount' => $currencyExchangeAmount
            ], Response::HTTP_OK);
        } catch (ConstraintException $e) {

            return new Response([
                'message' => $e->getMessage()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (Throwable $e) {

            return new Response([
                'message' => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     * @throws RuntimeException
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {
        throw new RuntimeException('Not implemented.');
    }
}
