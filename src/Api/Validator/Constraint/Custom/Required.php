<?php

declare(strict_types=1);

namespace Api\Validator\Constraint\Custom;

use Api\Validator\Constraint\ConstraintException;
use Api\Validator\Constraint\ConstraintInterface;
use Api\Validator\Constraint\ConstraintTrait;

final class Required implements ConstraintInterface
{
    use ConstraintTrait;

    /**
     * @return void
     * @throws ConstraintException
     */
    public function validate(): void
    {
        $value = $this->getValue();

        if (empty($value)) {
            throw new ConstraintException(
                sprintf('Required validation exception for field "%s". Field must be present and not empty.', $this->getName())
            );
        }
    }
}