<?php

declare(strict_types=1);

namespace Api\Validator\Constraint;

/**
 * ConstraintTrait delivers common functionality for partial implementation of ConstraintInterface.
 */
trait ConstraintTrait
{
    /**
     * @var string
     */
    private $name;

    /**
     * Value to validate.
     * @var mixed|null
     */
    private $value;

    /**
     * @param string $name
     * @param $value
     */
    public function __construct(string $name, $value = null)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }
}