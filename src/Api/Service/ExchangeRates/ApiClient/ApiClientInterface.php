<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\ApiClient;

/**
 * Api client interface for exchange rates webservice.
 */
interface ApiClientInterface
{
    /**
     * Method gets current NBP rates for currencies.
     * @return array
     */
    public function getExchangeRatesTable(): array;
}