#!/usr/bin/env bash

function installPackages() {
    apk update
    apk upgrade
    apk add --no-cache --progress \
        bash \
        tzdata
}

installPackages