<?php

declare(strict_types=1);

namespace Api\Dto;

use Api\Validator\Constraint\ConstraintInterface;
use Api\Validator\Constraint\Custom\Amount;
use Api\Validator\Constraint\Custom\Currency;
use Api\Validator\Constraint\Custom\Required;
use Api\Validator\ValidatableInterface;

/**
 * Representation of the input data for currency exchange calculator.
 */
class CalculatorExchangeDto implements ValidatableInterface
{
    /**
     * @var string|null
     */
    private $fromCurrency;

    /**
     * @var string|null
     */
    private $toCurrency;

    /**
     * @var string|null
     */
    private $amount;

    /**
     * @return string|null
     */
    public function getFromCurrency(): ?string
    {
        return $this->fromCurrency;
    }

    /**
     * @param string|null $fromCurrency
     * @return $this
     */
    public function setFromCurrency(?string $fromCurrency): self
    {
        $this->fromCurrency = $fromCurrency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getToCurrency(): ?string
    {
        return $this->toCurrency;
    }

    /**
     * @param string|null $toCurrency
     * @return $this
     */
    public function setToCurrency(?string $toCurrency): self
    {
        $this->toCurrency = $toCurrency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param string|null $amount
     * @return $this
     */
    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return ConstraintInterface[]
     */
    public function validationConstraints(): array
    {
        return [
            new Required(
                'fromCurrency',
                $this->getFromCurrency()
            ),
            new Currency(
                'fromCurrency',
                $this->getFromCurrency()
            ),
            new Required(
                'toCurrency',
                $this->getToCurrency()
            ),
            new Currency(
                'toCurrency',
                $this->getToCurrency()
            ),
            new Amount(
                'amount',
                $this->getAmount()
            )
        ];
    }
}