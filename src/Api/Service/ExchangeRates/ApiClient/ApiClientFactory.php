<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\ApiClient;

use Symfony\Component\HttpClient\HttpClient;

class ApiClientFactory
{
    /**
     * @var string
     */
    protected const API_URL = 'https://api.nbp.pl';

    /**
     * @var array
     */
    protected const API_REQUEST_HEADERS = [
        'Accept' => 'application/json'
    ];

    /**
     * @return ApiClientInterface
     */
    public static function create(): ApiClientInterface
    {
        return new ApiClient(
            HttpClient::create([
                'http_version' => '1.1',
                'base_uri' => static::API_URL,
                'headers' => static::API_REQUEST_HEADERS
            ])
        );
    }
}