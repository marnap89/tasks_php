<?php

declare(strict_types=1);

namespace Api\Validator;

use Api\Validator\Constraint\ConstraintException;

/**
 * ValidatorInterface defines public interface for validators.
 */
interface ValidatorInterface
{
    /**
     * @param ValidatableInterface $validatable
     * @return void
     * @throws ConstraintException
     */
    public function validate(ValidatableInterface $validatable): void;
}