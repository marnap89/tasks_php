<?php

declare(strict_types=1);

namespace Api\Validator\Constraint\Custom;

use Api\Validator\Constraint\ConstraintException;
use Api\Validator\Constraint\ConstraintInterface;
use Api\Validator\Constraint\ConstraintTrait;

final class Currency implements ConstraintInterface
{
    use ConstraintTrait;

    /**
     * Expected length for currency code.
     * @var int
     */
    private const CURRENCY_CODE_LENGTH = 3;

    /**
     * @return void
     * @throws ConstraintException
     */
    public function validate(): void
    {
        $currencyCode = $this->getValue();

        if ($currencyCode === null) {
            return;
        }

        if (strlen($currencyCode) < self::CURRENCY_CODE_LENGTH || strlen($currencyCode) > self::CURRENCY_CODE_LENGTH) {
            $errorMessage = 'Invalid currency code length.';
        } elseif (is_numeric($currencyCode)) {
            $errorMessage = 'Currency code should contain only letters.';
        }

        if (isset($errorMessage)) {
            throw new ConstraintException(
                sprintf('Currency validation exception for field "%s". %s', $this->getName(), $errorMessage)
            );
        }
    }
}