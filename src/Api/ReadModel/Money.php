<?php

declare(strict_types=1);

namespace Api\ReadModel;

/**
 * Representation of the money.
 */
class Money implements MoneyInterface
{
    /**
     * @var string
     */
    private $amount;

    /**
     * @var CurrencyInterface
     */
    private $currency;

    /**
     * @param string $amount
     * @param CurrencyInterface $currency
     */
    public function __construct(string $amount, CurrencyInterface $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): CurrencyInterface
    {
        return $this->currency;
    }
}