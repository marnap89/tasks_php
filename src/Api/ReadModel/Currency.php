<?php

declare(strict_types=1);

namespace Api\ReadModel;

use DateTimeInterface;

/**
 * Representation of the currency.
 */
class Currency implements CurrencyInterface
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $rate;

    /**
     * @var DateTimeInterface
     */
    private $effectiveDate;

    /**
     * @param string $code
     * @param string $name
     * @param string $rate
     * @param DateTimeInterface $effectiveDate
     */
    public function __construct(string $code, string $name, string $rate, DateTimeInterface $effectiveDate)
    {
        $this->code = $code;
        $this->name = $name;
        $this->rate = $rate;
        $this->effectiveDate = $effectiveDate;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRate(): string
    {
        return $this->rate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getEffectiveDate(): DateTimeInterface
    {
        return $this->effectiveDate;
    }
}