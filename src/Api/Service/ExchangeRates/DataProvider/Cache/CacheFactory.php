<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\DataProvider\Cache;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * CacheFactory creates instances of CacheInterfaces.
 */
class CacheFactory
{
    /**
     * @param string $namespace
     * @param string $cacheDir
     * @return CacheInterface
     */
    public static function create(string $namespace, string $cacheDir): CacheInterface
    {
        return new FilesystemAdapter(
            $namespace,
            3600,
            $cacheDir
        );
    }
}