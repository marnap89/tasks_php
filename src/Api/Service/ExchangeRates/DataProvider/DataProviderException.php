<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\DataProvider;

use Exception;

final class DataProviderException extends Exception
{

}