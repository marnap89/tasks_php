<?php

declare(strict_types=1);

namespace Api\ReadModel;

/**
 * MoneyInterface defines public interface for Money read model.
 *
 * MoneyInterface interface was created for apply principle of stable abstraction.
 * In case of missing this interface PHPMetric shows violation under Api\ReadModel namespace.
 * In real life, the usefulness of this interface is debatable in value objects.
 */
interface MoneyInterface
{
    /**
     * Amount of given currency.
     * @return string
     */
    public function getAmount(): string;

    /**
     * @return CurrencyInterface
     */
    public function getCurrency(): CurrencyInterface;
}