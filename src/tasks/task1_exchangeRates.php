<?php

use Api\ExchangeRates\CalculatorController;
use Api\Service\ExchangeRates\ApiClient\ApiClientFactory;
use Api\Service\ExchangeRates\Calculator\Calculator;
use Api\Service\ExchangeRates\DataProvider\DataProvider;
use Api\Service\ExchangeRates\DataProvider\Cache\CacheFactory;
use Api\Validator\Validator;

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    5
 * Actual Development Time [in hours]:      4.5 for main task + refactor dictated by PHPMetrics + 1 for dockerized local environment.
 * Your thoughts and comments:              It would be great to use an external libraries (e.g. PHPUnit or Codeception, Structurizr - for C4 diagrams)
 *                                          and newer version of PHP. PHP 7.2 is not maintained anymore :(
 * ============================================
 *
 * INPUT
 *
 * These parameters can also be set as a query params.
 * If query params are present, the input params below will be overwritten.
 *
 * Comment / uncomment any input set for testing :P
 */
$inputParams = array_merge(
//    // RESULT: {"message":"Amount validation exception for field \"amount\". Given amount is too small. We are sorry, but you are too poor."}
//    [
//        'fromCurrency' => 'USD',
//        'toCurrency' => 'EUR',
//        'amount' => 0,
//    ],

//    // RESULT: {"message":"Amount validation exception for field \"amount\". Given amount of money is too much. Contact me to calculate larger amount of money for additional payment!"}
//    [
//        'fromCurrency' => 'USD',
//        'toCurrency' => 'EUR',
//        'amount' => 150000000,
//    ],

//    // RESULT: {"message":"Required validation exception for field \"fromCurrency\". Field must be present and not empty."}
//    [
//        'fromCurrency' => '',
//        'toCurrency' => 'USD',
//        'amount' => 150,
//    ],

//    // RESULT: {"resulting_rate":"1.13285","resulting_amount":"14.16071"}
//    [
//        'fromCurrency' => 'EUR',
//        'toCurrency' => 'USD',
//        'amount' => 12.5,
//    ],

//    // RESULT: {"resulting_rate":"0.15617","resulting_amount":"312.49942"}
//    [
//        'fromCurrency' => 'NOK',
//        'toCurrency' => 'AUD',
//        'amount' => 2001,
//    ],

//    // RESULT: {"resulting_rate":"6.40321","resulting_amount":"12812.82682"}
//    [
//        'fromCurrency' => 'AUD',
//        'toCurrency' => 'NOK',
//        'amount' => 2001,
//    ],

//    // RESULT: {"resulting_rate":"0.08177"}
//    [
//        'fromCurrency' => 'PHP',
//        'toCurrency' => 'MYR',
//    ],

//    // RESULT: {"message":"Currency validation exception for field \"fromCurrency\". Invalid currency code length."}
//    [
//        'fromCurrency' => 'UNKNOWN',
//        'toCurrency' => 'CLP',
//    ],

    // RESULT: {"resulting_rate":"0.00005","resulting_amount":"0.00050"}
    [
        'fromCurrency' => 'IDR',
        'toCurrency' => 'XDR',
        'amount' => 10
    ],

    $_GET);

/**
 * Absolute path for cache directory inside docker container.
 */
$cacheDir = '/srv/app/var/cache';

/**
 * Action
 */
$controller = new CalculatorController(
    new Validator(),
    new DataProvider(
        ApiClientFactory::create(),
        CacheFactory::create('nbpRates', $cacheDir)
    ),
    new Calculator()
);
$response = $controller->calculateCurrentExchangeRateAction($inputParams);

/**
 * RESPONSE
 */
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
