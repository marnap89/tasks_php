<?php

declare(strict_types=1);

namespace Api\Validator;

use Api\Validator\Constraint\ConstraintInterface;

/**
 * ValidatableInterface defines public interface for objects that should be validated.
 */
interface ValidatableInterface
{
    /**
     * @return ConstraintInterface[]
     */
    public function validationConstraints(): array;
}