# Running App

Application was containerized because of two reasons:
 - I wanted to make sure that we both have installed the same needed extensions
 - For my own purpose. I didn't want to install old packages on my local machine :)

## Before run

You need to check if you have the latest versions of docker and docker-compose.

```shell
$ docker -v
Docker version 20.10.10, build b485636
```

```shell
$ docker-compose -v
docker-compose version 1.29.2, build 5becea4c
```

## Build and run

All you have to do is to type:

```shell
docker-compose up -d
```
If this is your first run, the `nginx` and `php` images will be built before running containers.
When your containers are up, enter to the `tasks_php`:

```shell
docker exec -it tasks_php bash
```

In the shell you can prepare an app. For instance install composer dependencies.

## Final thoughts

This is a very simple containerization only for running an app on development machine.
I didn't want to waste time on a more extensive version of the dockerization.
Feel free to contact me if you'll have any questions :)