#!/usr/bin/env bash

function installPackages() {
    apk update
    apk upgrade
    apk add --no-cache --progress \
        icu-dev \
        bash \
        composer \
        git \
        tzdata

    composer -qn self-update --2
}

installPackages