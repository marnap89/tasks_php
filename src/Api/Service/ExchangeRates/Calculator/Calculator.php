<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\Calculator;

use Api\ReadModel\Currency;
use Api\ReadModel\CurrencyInterface;
use Api\ReadModel\MoneyInterface;

/**
 * Calculator performs calculations on the Money and Currency objects using bcmath functions.
 *
 * If the application had more calculators, I would consider renaming this class to for example BcMathCalculator.
 */
final class Calculator implements CalculatorInterface
{
    /**
     * Precision scale for calculations.
     * @var int
     */
    private const PRECISION_RESULT = 5;

    /**
     * @param CurrencyInterface $currencyFrom
     * @param CurrencyInterface $currencyTo
     * @return string
     */
    public function calculateExchangeRate(CurrencyInterface $currencyFrom, CurrencyInterface $currencyTo): string
    {
        return bcdiv(
            $currencyFrom->getRate(),
            $currencyTo->getRate(),
            self::PRECISION_RESULT
        );
    }

    /**
     * @param MoneyInterface $moneyFrom
     * @param Currency $currencyTo
     * @return string
     */
    public function calculateExchangeCurrencyAmount(MoneyInterface $moneyFrom, CurrencyInterface $currencyTo): string
    {
        return bcmul(
            $moneyFrom->getAmount(),
            bcdiv(
                $moneyFrom->getCurrency()->getRate(),
                $currencyTo->getRate(),
                self::PRECISION_RESULT * 2 // Multiply precision for correct result.
            ),
            self::PRECISION_RESULT
        );
    }
}