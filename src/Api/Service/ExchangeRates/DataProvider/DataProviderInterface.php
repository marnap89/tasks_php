<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\DataProvider;

use Api\ReadModel\Currency;

/**
 * DataProviderInterface defines public interface for currency exchange rates providers.
 */
interface DataProviderInterface
{
    /**
     * @param string $currencyCode
     * @return Currency
     * @throws DataProviderException
     */
    public function provideCurrencyRateFor(string $currencyCode): Currency;
}