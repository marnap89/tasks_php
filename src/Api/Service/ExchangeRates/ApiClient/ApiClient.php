<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\ApiClient;

use Api\ReadModel\Currency;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Api client for exchange rates webservice.
 *
 * If the application had more api client classes, I would consider renaming this class to for example NbpApiClient.
 */
final class ApiClient implements ApiClientInterface
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @param HttpClientInterface $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return array
     * @throws ApiClientException
     */
    public function getExchangeRatesTable(): array
    {
        try {
            $apiResponse = $this->httpClient->request(Request::METHOD_GET, '/api/exchangerates/tables/A/');

            return $this->parseResponse($apiResponse->getContent());
        } catch (\Throwable $e) {
            throw new ApiClientException(
                sprintf('Unable to perform request. Reason: %s', $e->getMessage())
            );
        }
    }

    private function parseResponse(string $responseContent): array
    {
        $responseDecoded = \json_decode($responseContent);

        $currencyEffectiveDate = $responseDecoded[0]->effectiveDate;
        $currencyEffectiveDateTime = \DateTimeImmutable::createFromFormat('Y-m-d', $currencyEffectiveDate)
            ->setTime(0, 0);

        $currencyRatesArray = [
            'effectiveDate' => $currencyEffectiveDateTime,
            'rates' => []
        ];
        foreach ($responseDecoded[0]->rates as $currencyRates) {
            $currencyRatesArray['rates'][$currencyRates->code] = new Currency(
                $currencyRates->code,
                $currencyRates->currency,
                (string) $currencyRates->mid,
                $currencyEffectiveDateTime
            );
        }

        return $currencyRatesArray;
    }
}