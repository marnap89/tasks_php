#!/usr/bin/env bash

function installModules() {
    docker-php-ext-install -j"$(nproc)" \
        bcmath \
        intl
}

installModules