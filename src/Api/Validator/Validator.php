<?php

declare(strict_types=1);

namespace Api\Validator;

use Api\Validator\Constraint\ConstraintInterface;

/**
 * Very simple validator class.
 */
final class Validator implements ValidatorInterface
{
    /**
     * @param ValidatableInterface $validatable
     * @return void
     * @throws ValidationException
     */
    public function validate(ValidatableInterface $validatable): void
    {
        $constraintsCollection = $validatable->validationConstraints();

        /** @var ConstraintInterface $constraint */
        foreach ($constraintsCollection as $constraint) {
            $constraint->validate();
        }
    }
}