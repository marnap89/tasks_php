<?php

declare(strict_types=1);

namespace Api;

/**
 * Response class represent response object from controller's actions.
 *
 * I decided to create my own Response class and not use as a parent \Symfony\Component\HttpFoundation\Response.
 * For given task, the expected interface is very simple and lightweight. There is no necessary to involve any heavy object
 * even if it defines http code constants.
 */
class Response implements ResponseInterface
{
    /**
     * Http code 200 means that everything goes correctly and the expected response is returned.
     * @type int
     */
    public const HTTP_OK = 200;

    /**
     * Http code 422 means that there was a validation problem and provided input data was incorrect.
     * @type int
     */
    public const HTTP_UNPROCESSABLE_ENTITY = 422;

    /**
     * Http code 500 means that there was a server error.
     * @type int
     */
    public const HTTP_INTERNAL_SERVER_ERROR = 500;

    /**
     * @var array
     */
    private $content;

    /**
     * @var int
     */
    private $httpCode;

    /**
     * @param array $content
     * @param int $httpCode
     */
    public function __construct(array $content, int $httpCode)
    {
        $this->content = $content;
        $this->httpCode = $httpCode;
    }

    /**
     * @param bool $jsonEncoded
     * @return array|string
     */
    public function getContent(bool $jsonEncoded = true)
    {
        $content = array_filter($this->content);

        if ($jsonEncoded === true) {
            return json_encode($content);
        }

        return $content;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}