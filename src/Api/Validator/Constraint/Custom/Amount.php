<?php

declare(strict_types=1);

namespace Api\Validator\Constraint\Custom;

use Api\Validator\Constraint\ConstraintException;
use Api\Validator\Constraint\ConstraintInterface;
use Api\Validator\Constraint\ConstraintTrait;

final class Amount implements ConstraintInterface
{
    use ConstraintTrait;

    /**
     * Expected minimal value for amount.
     * @var int
     */
    private const AMOUNT_MINIMAL_VALUE = 1;

    /**
     * Expected maximal value for amount.
     * It is not dictated by technical limitation, but only my choice :)
     * @var int
     */
    private const AMOUNT_MAXIMAL_VALUE = 100000000;

    /**
     * @return void
     * @throws ConstraintException
     */
    public function validate(): void
    {
        $amount = $this->getValue();

        if ($amount === null) {
            return;
        }

        if ($amount < self::AMOUNT_MINIMAL_VALUE) {
            $errorMessage = 'Given amount is too small. We are sorry, but you are too poor.';
        } elseif ($amount > self::AMOUNT_MAXIMAL_VALUE) {
            $errorMessage = 'Given amount of money is too much. ' .
                'Contact me to calculate larger amount of money for additional payment!';
        }

        if (isset($errorMessage)) {
            throw new ConstraintException(
                sprintf('Amount validation exception for field "%s". %s', $this->getName(), $errorMessage)
            );
        }
    }
}