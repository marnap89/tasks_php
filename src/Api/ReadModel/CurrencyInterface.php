<?php

declare(strict_types=1);

namespace Api\ReadModel;

use DateTimeInterface;

/**
 * CurrencyInterface defines public interface for Currency read model.
 *
 * CurrencyInterface interface was created for apply principle of stable abstraction.
 * In case of missing this interface PHPMetric shows violation under Api\ReadModel namespace.
 * In real life, the usefulness of this interface is debatable in value objects.
 */
interface CurrencyInterface
{
    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getRate(): string;

    /**
     * @return DateTimeInterface
     */
    public function getEffectiveDate(): DateTimeInterface;
}