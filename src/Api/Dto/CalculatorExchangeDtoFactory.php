<?php

declare(strict_types=1);

namespace Api\Dto;

/**
 * Factory for CalculatorExchangePayload.
 */
class CalculatorExchangeDtoFactory
{
    /**
     * @param array $payload
     * @return CalculatorExchangeDto
     */
    public static function create(array $payload): CalculatorExchangeDto
    {
        $fromCurrency = static::filterPayloadParam($payload['fromCurrency'] ?? null);
        $toCurrency = static::filterPayloadParam($payload['toCurrency'] ?? null);
        $amount = static::filterPayloadParam($payload['amount'] ?? null);

        return (new CalculatorExchangeDto())
            ->setFromCurrency($fromCurrency)
            ->setToCurrency($toCurrency)
            ->setAmount($amount);
    }

    /**
     * @param mixed|null $param
     * @return string|null
     */
    private static function filterPayloadParam($param = null): ?string
    {
        if ($param !== null) {
            $param = trim(htmlspecialchars((string) $param));
            if (!is_numeric($param)) {
                return strtoupper($param);
            }

            return $param;
        }

        return null;
    }
}