<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\ApiClient;

use Exception;

final class ApiClientException extends Exception
{

}