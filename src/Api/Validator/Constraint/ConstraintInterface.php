<?php

declare(strict_types=1);

namespace Api\Validator\Constraint;

/**
 * ConstraintInterface defines public interface for constraint objects.
 */
interface ConstraintInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return mixed|null
     */
    public function getValue();

    /**
     * @return void
     * @throws ConstraintException
     */
    public function validate(): void;
}