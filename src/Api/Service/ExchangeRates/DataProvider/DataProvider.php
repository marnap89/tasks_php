<?php

declare(strict_types=1);

namespace Api\Service\ExchangeRates\DataProvider;

use Api\ReadModel\Currency;
use Api\Service\ExchangeRates\ApiClient\ApiClientInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use DateTimeInterface;
use DateTimeImmutable;
use Exception;

/**
 * DataProvider class provides currency rates from NBP api or from cache if was already warmed :)
 *
 * Why do I inject CacheInterface in DataProviderInterface as a dependency?
 *      In my opinion putting cache component into 'ApiClient' classes is a bad approach.
 *      ApiClient classes should not have any control logic.
 *      The task of these classes is to communicate with an external service and return data. Nothing more.
 */
final class DataProvider implements DataProviderInterface
{
    /**
     * Beginning hour for refreshing api data. (NBP conditions)
     * @var string
     */
    private const BEGINNING_REFRESH_DATA_HOUR = '11:45:00';

    /**
     * Numeric representation of Friday.
     * @var int
     */
    private const LAST_WORKING_DAY_NUMBER = 5;

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param ApiClientInterface $apiClient
     * @param CacheInterface $cache
     */
    public function __construct(ApiClientInterface $apiClient, CacheInterface $cache)
    {
        $this->apiClient = $apiClient;
        $this->cache = $cache;
    }

    /**
     * @param string $currencyCode
     * @return Currency
     * @throws DataProviderException
     */
    public function provideCurrencyRateFor(string $currencyCode): Currency
    {
        $currency = $this->provideCurrencyRateTable()['rates'][$currencyCode] ?? null;
        if ($currency !== null) {
            return $currency;
        }

        throw new DataProviderException(
            sprintf('Unable to find currency: %s.', $currencyCode)
        );
    }

    /**
     * @return array
     * @throws DataProviderException
     */
    private function provideCurrencyRateTable(): array
    {
        $cacheKey = 'currencyRateTable';

        try {
            return $this->cache->get('currencyRateTable', function (ItemInterface $item) {
                $currencyRatesTable = $this->apiClient->getExchangeRatesTable();

                $item->expiresAt($this->calculateExpiration($currencyRatesTable['effectiveDate']));

                return $currencyRatesTable;
            });
        } catch (InvalidArgumentException|\Throwable $e) {
            throw new DataProviderException(
                sprintf('Unable to fetch data from cache: %s', $e->getMessage())
            );
        }
    }

    /**
     * Method calculates the closest DateTime to invalidate cache.
     * The data may change only in working days of the week from 11:45:00 (to 12:15:00)
     *
     * @todo Find a way to simplify the method.
     *
     * @param DateTimeInterface $effectiveDateTime
     * @return DateTimeInterface
     * @throws Exception
     */
    private function calculateExpiration(\DateTimeInterface $effectiveDateTime): DateTimeInterface
    {
        $actualDateTime = new DateTimeImmutable();
        $actualWeekDay = $actualDateTime->format('N');

        if ($actualDateTime->format('Ymd') === $effectiveDateTime->format('Ymd')) {
            return new DateTimeImmutable(($actualWeekDay >= self::LAST_WORKING_DAY_NUMBER ? 'next Monday ' : 'tomorrow ') . self::BEGINNING_REFRESH_DATA_HOUR);
        }

        if ($actualWeekDay > self::LAST_WORKING_DAY_NUMBER) {
            return new DateTimeImmutable('next Monday ' . self::BEGINNING_REFRESH_DATA_HOUR);
        }

        $todayBeginningDateTime = new DateTimeImmutable('today ' . self::BEGINNING_REFRESH_DATA_HOUR);
        if ($actualDateTime < $todayBeginningDateTime) {
            return $todayBeginningDateTime;
        }

        return $actualDateTime->modify('+15 minutes');
    }
}